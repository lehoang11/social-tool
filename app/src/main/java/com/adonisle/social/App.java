package com.adonisle.social;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.net.UnknownHostException;

@SpringBootApplication
@ComponentScan({
        "com.adonisle.social"

})
@EnableAutoConfiguration(exclude = {JpaRepositoriesAutoConfiguration.class})
@Slf4j
@EnableScheduling
public class App {
    public static void main(String[] args) throws UnknownHostException {
        log.info("Start social tool application");
        SpringApplication.run(App.class, args);

    }

}
