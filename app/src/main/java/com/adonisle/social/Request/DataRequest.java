package com.adonisle.social.Request;

import java.util.Date;

public class DataRequest {

    private Date dateTime;

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
