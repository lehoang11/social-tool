package com.adonisle.social.common;


import java.util.Arrays;
import java.util.List;

public interface Constants {

    interface GroupChannel {
        String ADNET = "ADNET";
        String FACEBOOK = "FB";
        String CHATBOT = "CHATBOT";
        String EMAIL = "EMAIL";
        String GOOGLE = "GOOGLE";
        String ENGAGEMENT = "ENGAGEMENT"; //zalo
        String SMS = "SMS";
        String COCCOC = "COCCOC";
        String JRA_CHIPHI = "JRA_CHIPHI";
    }

    List<String> GroupChannelList = Arrays.asList("ADNET", "FB",
            "CHATBOT", "EMAIL", "GOOGLE", "ENGAGEMENT","SMS","COCCOC","JRA_CHIPHI");

}

