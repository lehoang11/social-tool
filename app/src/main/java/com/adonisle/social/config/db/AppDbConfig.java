package com.adonisle.social.config.db;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "appEntityManager",
        transactionManagerRef = "appTransactionManager",
        basePackages = {
                "com.adonisle.social.repository"
        }
)
public class AppDbConfig extends DbConfig {

    private String[] ENTITYMANAGER_PACKAGES_TO_SCAN = {"com.adonisle.social.model"};

    {
        DB_URL = "app.db.url";
        DB_USER = "app.db.user";
        DB_PASSWORD = "app.db.password";
    }

    @Primary
    @Bean(name = "appDataSource")
    public DataSource appDataSource() {
        return this.buildDataSource();
    }

    @Primary
    @Bean
    public PlatformTransactionManager appTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(appEntityManager().getObject());
        return transactionManager;
    }

    @Primary
    @Bean(name = "appEntityManager")
    public LocalContainerEntityManagerFactoryBean appEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(appDataSource());
        em.setPackagesToScan(ENTITYMANAGER_PACKAGES_TO_SCAN);
        em.setJpaVendorAdapter(vendorAdaptor());
        em.setPersistenceUnitName("app");
        return em;
    }
}

