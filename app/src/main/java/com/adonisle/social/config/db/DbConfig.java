package com.adonisle.social.config.db;


import com.mchange.v2.c3p0.ComboPooledDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

@Slf4j
public class DbConfig {
    protected String DB_URL;
    protected String DB_USER;
    protected String DB_PASSWORD;
    protected String DB_DRIVER = "db.driver";
    protected String DB_DIALECT = "db.dialect";
    protected String SHOW_SQL = "show.sql";
    protected int MIN_POOL = 3;
    protected int MAX_POOL = 15;

    @Autowired
    protected Environment env;

    protected HibernateJpaVendorAdapter vendorAdaptor() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabasePlatform(env.getProperty(DB_DIALECT));
        vendorAdapter.setShowSql(Boolean.valueOf(env.getProperty(SHOW_SQL)));
        return vendorAdapter;
    }

    protected DataSource buildDataSource() {
        ComboPooledDataSource cpds = new ComboPooledDataSource();
        try {
            cpds.setDriverClass(env.getProperty(DB_DRIVER));
        } catch (PropertyVetoException e) {
            log.error("GET DRIVER CLASS ERROR !");
        }
        cpds.setJdbcUrl(env.getProperty(DB_URL));
        cpds.setUser(env.getProperty(DB_USER));
        cpds.setPassword(env.getProperty(DB_PASSWORD));
        cpds.setMinPoolSize(MIN_POOL);
        cpds.setMaxPoolSize(MAX_POOL);
        cpds.setAcquireIncrement(3);
        cpds.setTestConnectionOnCheckin(true);
        cpds.setIdleConnectionTestPeriod(300);
        cpds.setMaxIdleTimeExcessConnections(240);
        cpds.setPreferredTestQuery("SELECT 1");
        return cpds;
    }
}


