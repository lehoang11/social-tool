package com.adonisle.social.controller;


import com.adonisle.social.service.youtube.KindPlanService;
import com.adonisle.social.service.youtube.TrafficSourceService;
import com.adonisle.social.service.youtube.VideoService;
import com.adonisle.social.service.youtube.YoutubeSearchService;
import com.adonisle.social.utils.CommonUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@Slf4j
@RestController
@RequestMapping("/job")
@Api(value = "home", description = "home Information", produces = MediaType.APPLICATION_JSON_VALUE)
public class HomeController {

    @Autowired
    VideoService videoService;
    @Autowired
    TrafficSourceService trafficSourceService;
    @Autowired
    YoutubeSearchService youtubeSearchService;

    @Autowired
    KindPlanService kindPlanService;

    @RequestMapping(value = "run", method = RequestMethod.GET)
    @ApiOperation(value = " API job run ", response = String.class)
    public  String home()  {
        log.info("Begin run job: ");
        String data = "Đã chay lúc : ".concat(CommonUtils.getCurrentDateFullStr() );
        videoService.ScheduleVideo();
        trafficSourceService.ScheduleTrafficSource();
        youtubeSearchService.ScheduleYoutubeSearch();
        kindPlanService.ScheduleKindPlan();
        return data;
    }


}
