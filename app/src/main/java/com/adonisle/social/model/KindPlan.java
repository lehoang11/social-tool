package com.adonisle.social.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "kind_plan")
public class KindPlan {

    @Id
    private Long id;

    @Column(name="date")
    private Date date;

    @Column(name="view")
    private Long view;

    @Column(name="watchtime")
    private Long watchtime;

    @Column(name="comment")
    private Long comment;

    @Column(name="subscribe")
    private Long subscribe;

    @Column(name="likes")
    private Long likes;

    @Column(name="gc1")
    private String gc1;

    @Column(name="gc2")
    private String gc2;

    @Column(name="gc3")
    private String gc3;

    @Column(name="file_id")
    private String fileId;

    @Column(name="file_name")
    private String fileName;



}
