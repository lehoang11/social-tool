package com.adonisle.social.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;


@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "traffic_source")
public class TrafficSource {

    @Id
    private Long id;

    @Column(name="date")
    private Date date;

    @Column(name="code_video")
    private String codeVideo;

    @Column(name="traffic_source_type")
    private String trafficSourceType;

    @Column(name="watch_time_minutes")
    private Float watchTimeMinutes;

    @Column(name="views")
    private Long views;

    @Column(name="average_view_duration")
    private Float averageViewDuration;

    @Column(name="video_thumbnail_impressions")
    private Float videoThumbnailImpressions;

    @Column(name="video_thumbnail_impressions_ctr")
    private Float videoThumbnailImpressionsCtr;

    @Column(name="file_id")
    private String fileId;

    @Column(name="file_name")
    private String fileName;


}
