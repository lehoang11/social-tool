package com.adonisle.social.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "video")
public class Video {

    @Id
    private Long id;

    @Column(name="date")
    private Date date;

    @Column(name="video")
    private String video;

    @Column(name="video_title")
    private String videoTitle;

    @Column(name="watch_time_minutes")
    private Float watchTimeMinutes;

    @Column(name="views")
    private Long views;

    @Column(name="subscribers")
    private Long subscribers;

    @Column(name="video_thumbnail_impressions")
    private Float videoThumbnailImpressions;

    @Column(name="video_thumbnail_impressions_ctr")
    private Float videoThumbnailImpressionsCtr;

    @Column(name="estimated_partner_revenue")
    private Float estimatedPartnerRevenue;

    @Column(name="file_id")
    private String fileId;

    @Column(name="file_name")
    private String fileName;

}
