package com.adonisle.social.model;

import javax.persistence.Column;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "youtube_search")
public class YoutubeSearch {

    @Id
    private Long id;

    @Column(name="date")
    private Date date;

    @Column(name="period")
    private String period;

    @Column(name="video")
    private String video;

    @Column(name="traffic_source_detail")
    private String trafficSourceDetail;

    @Column(name="referrer_type")
    private String referrerType;

    @Column(name="referrer_title")
    private String referrerTitle;

    @Column(name="watch_time_minutes")
    private Float watchTimeMinutes;

    @Column(name="views")
    private Long views;

    @Column(name="average_view_duration")
    private Float averageViewDuration;

    @Column(name="video_thumbnail_impressions")
    private Float videoThumbnailImpressions;

    @Column(name="video_thumbnail_impressions_ctr")
    private Float  videoThumbnailImpressionsCtr;

    @Column(name="file_id")
    private String fileId;

    @Column(name="file_name")
    private String fileName;


}
