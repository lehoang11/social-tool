package com.adonisle.social.repository;

import com.adonisle.social.model.TrafficSource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;


public interface TrafficSourceRepository extends JpaRepository<TrafficSource, Long> {

    @Modifying
    @Transactional
    @Query(value = "DELETE  FROM TrafficSource y WHERE y.date like CONCAT('%', :date ,'%') ")
    void DeleteData(@Param("date") String date);

    @Query(value = " SELECT distinct(file_name) FROM traffic_source ", nativeQuery = true)
    List<String> getFileNameAll();
}
