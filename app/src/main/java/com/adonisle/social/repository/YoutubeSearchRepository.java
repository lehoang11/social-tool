package com.adonisle.social.repository;

import com.adonisle.social.model.YoutubeSearch;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;


public interface YoutubeSearchRepository extends JpaRepository<YoutubeSearch, Long> {

    @Modifying
    @Transactional
    @Query(value = "DELETE  FROM YoutubeSearch y WHERE y.date like CONCAT('%', :date ,'%') ")
    void DeleteData(@Param("date") String date);

    @Query(value = " SELECT distinct(file_name) FROM youtube_search " , nativeQuery = true)
    List<String> getFileNameAll();
}
