package com.adonisle.social.service.schedule;

import com.adonisle.social.service.youtube.KindPlanService;
import com.adonisle.social.service.youtube.TrafficSourceService;
import com.adonisle.social.service.youtube.VideoService;
import com.adonisle.social.service.youtube.YoutubeSearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class SocialSchedule {

    @Autowired
    VideoService videoService;
    @Autowired
    YoutubeSearchService youtubeSearchService;

    @Autowired
    TrafficSourceService trafficSourceService;

    @Autowired
    KindPlanService kindPlanService;

    private static final Logger log = LoggerFactory.getLogger(SocialSchedule.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");


    //@Scheduled(fixedRate = 7200000)
    @Scheduled(cron = " 0 0 17 * * * ")
    public void CRJ() {
        log.info("The time is now {}", dateFormat.format(new Date()));
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_YEAR);
        log.info("The time is day {}", day );
        // Cron Job report

        videoService.ScheduleVideo();

        youtubeSearchService.ScheduleYoutubeSearch();

        trafficSourceService.ScheduleTrafficSource();

        kindPlanService.ScheduleKindPlan();




    }




}
