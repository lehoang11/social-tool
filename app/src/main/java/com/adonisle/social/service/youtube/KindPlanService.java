package com.adonisle.social.service.youtube;

public interface KindPlanService {

    void ScheduleKindPlan();

    void DeleteData(int day);
}
