package com.adonisle.social.service.youtube;

public interface TrafficSourceService {

    void ScheduleTrafficSource();

    void DeleteData(int day);
}
