package com.adonisle.social.service.youtube;

public interface YoutubeSearchService {

    void ScheduleYoutubeSearch();

    void DeleteData(int day);
}
