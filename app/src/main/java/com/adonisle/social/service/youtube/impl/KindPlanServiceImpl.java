package com.adonisle.social.service.youtube.impl;

import com.adonisle.social.model.KindPlan;
import com.adonisle.social.model.Video;
import com.adonisle.social.repository.KindPlanRepository;
import com.adonisle.social.repository.VideoRepository;
import com.adonisle.social.service.google.SheetsService;
import com.adonisle.social.service.youtube.KindPlanService;
import com.adonisle.social.utils.CommonUtils;
import com.google.api.services.drive.model.File;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.List;

import static com.adonisle.social.service.google.DriveService.getListFileInSubFolder;

@Service
@Slf4j
public class KindPlanServiceImpl implements KindPlanService {

    @Value("${drive.folder.id}")
    private String googleFolderIdParent;

    @Value("${drive.subfolder.kindPlan.name}")
    private String subFolderNameParent;

    @Value("${google.range.kindPlan.sheet}")
    private String range;

    @Autowired
    KindPlanRepository kindPlanRepository;

    @Override
    public void ScheduleKindPlan() {
        List<String> fileList = kindPlanRepository.getFileNameAll();
        try {
            List<File> listFile =  getListFileInSubFolder( googleFolderIdParent, subFolderNameParent);
            if (listFile == null) {
                log.info("file null" );
                return;
            }
            for (File fileExcell :listFile) {
//                String fileNameSub = fileExcell.getName().substring(0,10).trim();
//                String fileName = fileNameSub.trim();
                if (fileList.contains(fileExcell.getName().trim())){
                    continue;
                }
                List<List<Object>> data = SheetsService.getData(fileExcell.getId() ,range );
                if (data == null) {
                    log.info("data kindPlan null" );
                    continue;
                }
                log.info("get kindPlan data success ");

                int dayConvert = CommonUtils.convertDayFromFileName(fileExcell.getName());

                SaveCopyFromSheet(data, dayConvert , fileExcell.getId(), fileExcell.getName().trim());


            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        return;

    }

    @Override
    public void DeleteData(int day) {
        kindPlanRepository.DeleteData(CommonUtils.getCurrentDate_Format(day));
    }

    private void SaveCopyFromSheet(List<List<Object>> data, int day, String fileId, String fileName ){

        Date date = CommonUtils.getCurrentDateYMD(day);
        int count = -1;
        for (List item :data){
            count++;
            if (count == 0) continue;
            int si = item.size();
            if (si == 0) {
                continue;
            }
            KindPlan kindPlan = new KindPlan();
            kindPlan.setDate(date);
            kindPlan.setId(CommonUtils.GeneralId());

            if(si > 1) {
                if (item.get(1) != null) kindPlan.setView( CommonUtils.ConvertToLong( item.get(1)) );
            }
            if(si > 2) {
                if (item.get(2) != null) kindPlan.setWatchtime( CommonUtils.ConvertToLong( item.get(2)) );
            }
            if(si > 3) {
                if (item.get(3) != null) kindPlan.setComment( CommonUtils.ConvertToLong( item.get(3)) );
            }
            if(si > 4) {
                if (item.get(4) != null) kindPlan.setSubscribe( CommonUtils.ConvertToLong( item.get(4)) );
            }
            if(si > 5) {
                if (item.get(5) != null) kindPlan.setLikes( CommonUtils.ConvertToLong( item.get(5)) );
            }
            if(si > 6) {
                if (item.get(6) != null) kindPlan.setGc1( item.get(6).toString());
            }

            if(si > 7) {
                if (item.get(7) != null) kindPlan.setGc1( item.get(7).toString());
            }

            if(si > 8) {
                if (item.get(8) != null) kindPlan.setGc1( item.get(8).toString());
            }

            kindPlan.setFileId(fileId);
            kindPlan.setFileName(fileName);

            kindPlanRepository.save(kindPlan);
        }

    }


}
