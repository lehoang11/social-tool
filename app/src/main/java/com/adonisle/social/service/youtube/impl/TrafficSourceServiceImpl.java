package com.adonisle.social.service.youtube.impl;

import com.adonisle.social.model.TrafficSource;
import com.adonisle.social.repository.TrafficSourceRepository;
import com.adonisle.social.service.google.SheetsService;
import com.adonisle.social.service.youtube.TrafficSourceService;
import com.adonisle.social.utils.CommonUtils;
import com.google.api.services.drive.model.File;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.List;

import static com.adonisle.social.service.google.DriveService.getListFileInSubFolder;

@Service
@Slf4j
public class TrafficSourceServiceImpl implements TrafficSourceService {

    @Value("${drive.folder.id}")
    private String googleFolderIdParent;

    @Value("${drive.subfolder.trafficsource.name}")
    private String subFolderNameParent;

    @Value("${google.range.trafficsource.sheet}")
    private String range;

    @Autowired
    TrafficSourceRepository trafficSourceRepository;

    @Override
    public void ScheduleTrafficSource() {

        List<String> fileList = trafficSourceRepository.getFileNameAll();
        try {
            List<File> listFile =  getListFileInSubFolder( googleFolderIdParent, subFolderNameParent);
            if (listFile == null) {
                log.info("TrafficSource file null" );
                return;
            }
            for (File fileExcell :listFile) {
                if (fileList.contains(fileExcell.getName().trim())){
                    continue;
                }
                List<List<Object>> data = SheetsService.getData(fileExcell.getId() ,range );
                if (data == null) {
                    log.info("data TrafficSource null" );
                    continue;
                }
                log.info("get TrafficSource data success ");
                System.out.println("file name" + fileExcell.getName());
                int dayConvert = CommonUtils.convertDayFromFileName(fileExcell.getName());
                SaveCoppyFromSheet(data, dayConvert, fileExcell.getId(), fileExcell.getName().trim() );


            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        return;

    }

    @Override
    public void DeleteData(int day) {
        trafficSourceRepository.DeleteData(CommonUtils.getCurrentDate_Format(day));
    }


    private void SaveCoppyFromSheet(List<List<Object>> data, int day, String fileId, String fileName ){

        Date date = CommonUtils.getCurrentDateYMD(day);
        int count = -1;
        for (List item :data){
            count++;
            if (count == 0) continue;
            int si = item.size();
            if (si == 0) {
                continue;
            }
            TrafficSource trafficSource = new TrafficSource();
            trafficSource.setDate(date);
            trafficSource.setId(CommonUtils.GeneralId());
            if (item.get(1) != null) trafficSource.setCodeVideo( item.get(1).toString() );
            if (item.get(2) != null) trafficSource.setTrafficSourceType( item.get(2).toString() );
            if (item.get(3) != null) trafficSource.setWatchTimeMinutes( CommonUtils.ConvertToFloat( item.get(3) ));
            if (item.get(4) != null) trafficSource.setViews( CommonUtils.ConvertToLong(item.get(4)) );
            if(si > 5) {
                if (item.get(5) != null) trafficSource.setAverageViewDuration( CommonUtils.ConvertToFloat( item.get(5) ) );
            }
            if(si > 6) {
                if (item.get(6) != null) trafficSource.setVideoThumbnailImpressions( CommonUtils.ConvertToFloat( item.get(6) ) );
            }
            if(si > 7) {
                if (item.get(7) != null) trafficSource.setVideoThumbnailImpressionsCtr( CommonUtils.ConvertToFloat( item.get(7) ));
            }

            trafficSource.setFileId(fileId);
            trafficSource.setFileName(fileName);

            trafficSourceRepository.save(trafficSource);
        }

    }




}
