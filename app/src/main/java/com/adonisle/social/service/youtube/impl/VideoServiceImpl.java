package com.adonisle.social.service.youtube.impl;

import com.adonisle.social.model.Video;
import com.adonisle.social.repository.VideoRepository;
import com.adonisle.social.service.google.SheetsService;
import com.adonisle.social.service.youtube.VideoService;
import com.adonisle.social.utils.CommonUtils;
import com.google.api.services.drive.model.File;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.adonisle.social.service.google.DriveService.getListFileInSubFolder;

@Service
@Slf4j
public class VideoServiceImpl implements VideoService {

    @Value("${drive.folder.id}")
    private String googleFolderIdParent;

    @Value("${drive.subfolder.video.name}")
    private String subFolderNameParent;

    @Value("${google.range.video.sheet}")
    private String range;

    @Autowired
    VideoRepository videoRepository;

    @Override
    public void ScheduleVideo() {
        List<String> fileList = videoRepository.getFileNameAll();
        try {
            List<File> listFile =  getListFileInSubFolder( googleFolderIdParent, subFolderNameParent);
            if (listFile == null) {
                log.info("file null" );
                return;
            }
            for (File fileExcell :listFile) {
//                String fileNameSub = fileExcell.getName().substring(0,10).trim();
//                String fileName = fileNameSub.trim();
                if (fileList.contains(fileExcell.getName().trim())){
                    continue;
                }
                List<List<Object>> data = SheetsService.getData(fileExcell.getId() ,range );
                if (data == null) {
                    log.info("data video null" );
                    continue;
                }
                log.info("get video data success ");

                int dayConvert = CommonUtils.convertDayFromFileName(fileExcell.getName());

                SaveCoppyFromSheet(data, dayConvert , fileExcell.getId(), fileExcell.getName().trim());


            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        return;

    }

    @Override
    public void DeleteData(int day) {
        videoRepository.DeleteData(CommonUtils.getCurrentDate_Format(day));
    }

    private void SaveCoppyFromSheet(List<List<Object>> data, int day, String fileId, String fileName ){

        Date date = CommonUtils.getCurrentDateYMD(day);
        int count = -1;
        for (List item :data){
            count++;
            if (count == 0) continue;
            int si = item.size();
            if (si == 0) {
                continue;
            }
            Video  video = new Video();
            video.setDate(date);
            video.setId(CommonUtils.GeneralId());
            if (item.get(1) != null) video.setVideo( item.get(1).toString() );
            if (item.get(2) != null) video.setVideoTitle( item.get(2).toString() );

            if (item.get(3) != null) video.setViews( CommonUtils.ConvertToLong(item.get(3)) );
            if (item.get(4) != null) video.setWatchTimeMinutes( CommonUtils.ConvertToFloat(item.get(4)) ) ;
            if(si > 5) {
                if (item.get(5) != null) video.setSubscribers( CommonUtils.ConvertToLong(item.get(5)) );
            }
            if(si > 6) {
                if (item.get(6) != null) video.setVideoThumbnailImpressions( CommonUtils.ConvertToFloat( item.get(6)) );
            }
            if(si > 7) {
                if (item.get(7) != null) video.setVideoThumbnailImpressionsCtr( CommonUtils.ConvertToFloat( item.get(7)) );
            }
            if(si > 8) {
                if (item.get(8) != null) video.setEstimatedPartnerRevenue( CommonUtils.ConvertToFloat(item.get(8)) );
            }

            video.setFileId(fileId);
            video.setFileName(fileName);

            videoRepository.save(video);
        }

    }





}
