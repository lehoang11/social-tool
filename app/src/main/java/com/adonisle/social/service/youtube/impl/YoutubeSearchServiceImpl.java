package com.adonisle.social.service.youtube.impl;

import com.adonisle.social.model.YoutubeSearch;
import com.adonisle.social.repository.YoutubeSearchRepository;
import com.adonisle.social.service.google.SheetsService;
import com.adonisle.social.service.youtube.YoutubeSearchService;
import com.adonisle.social.utils.CommonUtils;
import com.google.api.services.drive.model.File;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.List;

import static com.adonisle.social.service.google.DriveService.getListFileInSubFolder;

@Service
@Slf4j
public class YoutubeSearchServiceImpl implements YoutubeSearchService {

    @Value("${drive.folder.id}")
    private String googleFolderIdParent;

    @Value("${drive.subfolder.youtubesearch.name}")
    private String subFolderNameParent;

    @Value("${google.range.youtubesearch.sheet}")
    private String range;

    @Autowired
    YoutubeSearchRepository youtubeSearchRepository;

    @Override
    public void ScheduleYoutubeSearch() {

        List<String> fileList = youtubeSearchRepository.getFileNameAll();
        try {
            List<File> listFile =  getListFileInSubFolder( googleFolderIdParent, subFolderNameParent);
            if (listFile == null) {
                log.info("YoutubeSearch file null" );
                return;
            }
            for (File fileExcell :listFile) {
                if (fileList.contains(fileExcell.getName().trim())){
                    continue;
                }
                List<List<Object>> data = SheetsService.getData(fileExcell.getId() ,range );
                if (data == null) {
                    log.info("data YoutubeSearch null" );
                    continue;
                }
                log.info("get YoutubeSearch data success ");
                int dayConvert = CommonUtils.convertDayFromFileName(fileExcell.getName());
                SaveCoppyFromSheet(data, dayConvert, fileExcell.getId(), fileExcell.getName().trim());


            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        return;

    }

    @Override
    public void DeleteData(int day) {
        String date = CommonUtils.getCurrentDate_Format(day);
        youtubeSearchRepository.DeleteData(date);
    }

    private void SaveCoppyFromSheet(List<List<Object>> data, int day, String fileId, String fileName){

        Date date = CommonUtils.getCurrentDateYMD(day);
        int count = -1;
        for (List item :data){
            count++;
            if (count == 0) continue;
            int si = item.size();
            if (si == 0) {
                continue;
            }
            YoutubeSearch youtubeSearch = new YoutubeSearch();
            youtubeSearch.setDate(date);
            youtubeSearch.setId(CommonUtils.GeneralId());
            if (item.get(1) != null) youtubeSearch.setPeriod( item.get(1).toString() );
            if (item.get(2) != null) youtubeSearch.setVideo( item.get(2).toString() );
            if (item.get(3) != null) youtubeSearch.setTrafficSourceDetail(item.get(3).toString() );
            if (item.get(4) != null) youtubeSearch.setReferrerType( item.get(4).toString() );
            if(si > 5) {
                if (item.get(5) != null) youtubeSearch.setReferrerTitle(item.get(5).toString());
            }
            if(si > 6) {
                if (item.get(6) != null) youtubeSearch.setWatchTimeMinutes( CommonUtils.ConvertToFloat(item.get(6)) );
            }
            if(si > 7) {
                if (item.get(7) != null) youtubeSearch.setViews( CommonUtils.ConvertToLong(item.get(7)));
            }
            if(si > 8) {
                if (item.get(8) != null) youtubeSearch.setAverageViewDuration( CommonUtils.ConvertToFloat(item.get(8)));
            }
            if(si > 9) {
                if (item.get(9) != null) youtubeSearch.setVideoThumbnailImpressions(   CommonUtils.ConvertToFloat(item.get(9)));
            }
            if(si > 10) {
                if (item.get(10) != null) youtubeSearch.setVideoThumbnailImpressionsCtr(  CommonUtils.ConvertToFloat(item.get(10)));
            }

            youtubeSearch.setFileId(fileId);
            youtubeSearch.setFileName(fileName);

            youtubeSearchRepository.save(youtubeSearch);
        }

    }





}
