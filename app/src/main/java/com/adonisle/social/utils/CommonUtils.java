package com.adonisle.social.utils;

import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class CommonUtils {


    public static String getCurrentDate(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static String getCurrentDate(int day){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static String getCurrentDate_Format(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd");
        Calendar cal = Calendar.getInstance();
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static String getCurrentDate_Format(int day){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static String getCurrentDateTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static TimeZone getCurrentDateTimeZone(int day){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        return cal.getTimeZone();
    }

    public static String getStartDateTimeStr(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0 );
        cal.set(Calendar.SECOND, 0);
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static String getStartDateTimeStr(int day){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0 );
        cal.set(Calendar.SECOND, 0);
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static String getEndDateTimeStr(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59 );
        cal.set(Calendar.SECOND, 59);
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static String getEndDateTimeStr(int day){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59 );
        cal.set(Calendar.SECOND, 59);
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static Date getStartDateTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0 );
        cal.set(Calendar.SECOND, 0);
        String dateFormat = formatter.format(cal.getTime());
        Date dt = Timestamp.valueOf(dateFormat);
        return dt;
    }

    public static Date getStartDateTime(int day){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0 );
        cal.set(Calendar.SECOND, 0);
        String dateFormat = formatter.format(cal.getTime());
        Date dt = Timestamp.valueOf(dateFormat);
        return dt;
    }


    public static Date getEndDateTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59 );
        cal.set(Calendar.SECOND, 59);
        String dateFormat = formatter.format(cal.getTime());
        Date dt = Timestamp.valueOf(dateFormat);
        return dt;
    }

    public static Date getEndDateTime(int day){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59 );
        cal.set(Calendar.SECOND, 59);
        String dateFormat = formatter.format(cal.getTime());
        Date dt = Timestamp.valueOf(dateFormat);
        return dt;
    }

    public static Date getCurrentDateYMD(int day){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        String dateFormat = formatter.format(cal.getTime());
        Date dt = Timestamp.valueOf(dateFormat);
        return dt;
    }

    public static Long getCurrentDateTimeL(int day){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        return cal.getTimeInMillis();
    }

    public static Long getStartDateTimeL(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0 );
        cal.set(Calendar.SECOND, 0);
        return cal.getTimeInMillis();
    }

    public static Long getStartDateTimeL(int day ){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0 );
        cal.set(Calendar.SECOND, 0);
        return cal.getTimeInMillis();
    }



    public static Long getEndDateTimeL(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59 );
        cal.set(Calendar.SECOND, 59);

        return cal.getTimeInMillis();
    }

    public static Long getEndDateTimeL(int day ){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59 );
        cal.set(Calendar.SECOND, 59);

        return cal.getTimeInMillis();
    }


    public static String getCurrentDateAndHour(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH");
        Calendar cal = Calendar.getInstance();
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static String getCurrentDateAndHour(int day){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static String getCurrentDateFullStr(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static String getCurrentDateFullStr(int day ){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        String dateFormat = formatter.format(cal.getTime());
        return dateFormat;
    }

    public static Date getCurrentDateFull(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String dateFormat = formatter.format(cal.getTime());
        Date dt = Timestamp.valueOf(dateFormat);
        return dt;
    }

    public static Date getCurrentDateFull(int day){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        String dateFormat = formatter.format(cal.getTime());
        Date dt = Timestamp.valueOf(dateFormat);
        return dt;
    }

    // get current
    public static Long getDateTimeL(int day ){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_YEAR, day);
        return cal.getTimeInMillis();
    }


    public static Long GeneralId(){
        String s = "";
        double d;
        for (int i = 1; i <= 15; i++) {
            d = Math.random() * 10;
            s = s + ((int)d);
            if (i % 4 == 0 && i != 16) {
                s = s;
            }
        }
        return Long.valueOf(s);
    }

    public static   Long ConvertToLong(Object o){
        Long l = 0l;
        if (StringUtils.isEmpty(o.toString())){
            return l;
        }
        String stringToConvert = String.valueOf(o);
        String s = stringToConvert;
        if(stringToConvert.contains(",")){
             s = stringToConvert.replace(",", "");
        }
        return  Long.parseLong(s);


    }

    public static   Float ConvertToFloat(Object o){
        Float l = 0f;
        if (StringUtils.isEmpty(o.toString())){
            return l;
        }
        String stringToConvert = String.valueOf(o);
        String s = stringToConvert;
        if(stringToConvert.contains(",")){
            s = stringToConvert.replace(",", "");
        }
        return Float.parseFloat(s);

    }

    public static int convertDayFromFileName(String fileName){
        String dateStr = fileName.substring(0,10).trim();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(fileName);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_YEAR);
    }


}
